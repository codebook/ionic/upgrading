# How to Upgrade Ionic to a Newer Release

First, upgrade the cli:

    npm i -g ionic@latest

Then, for each Ionic project, upgrade relevant modules. For instance,

    npm i --save ionic-angular@latest
    // ...

For some release, you may have to upgrade the app-scripts and/or other ionic tools. For example,

    npm i --save-dev @ionic/app-scripts@latest
    // ...


In addition, you might have to upgrade other relevant `@angular/xxx`, `@ionic/xxx`, and `@ionic-native/xxx` modules.



## References

* [Ionic-Angular 3.7 Upgrade Instructions](http://blog.ionic.io/updates-for-all-ionic-angular-3-7-and-more/)

